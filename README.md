--------------------------------------------------------------------------------
                               Twilio Authy
--------------------------------------------------------------------------------


The Twilio authy module provides integration with the Twilio authy API allowing for your Drupal site to integration Twilio anthy functionality.

https://github.com/twilio/authy-php

Installation
------------

1. Install the Twilio Authy module as per (http://drupal.org/node/895232/)
2. Download the Twilio php library from (https://github.com/twilio/authy-php).
3. Extract the library in your sites/all/libraries folder and rename the
   directory to 'twilio_authy'.
4. Visit 'admin/config/twilio-authy' and enter your Twilio Authy information
   found on the twilio console dashboard
