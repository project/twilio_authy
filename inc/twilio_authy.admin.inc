<?php
/**
 * Twilio authy admin settings.
 */

/**
 * Admin form for twilio authy.
 */
function twilio_authy_admin_form($form, &$form_state) {
  $form['twilio_authy_x_authy_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Authy API Key'),
    '#description' => t('The API Key can be found in the !here after clicking through to your Authy application.', array(
      '!here' => l(t('Authy section of the Twilio Console'), 'https://www.twilio.com/console/authy/applications', array(
        'attributes' => array(
          'target' => '_blank',
        ),
      )),
    )),
    '#default_value' => variable_get('twilio_authy_x_authy_api_key'),
  );
  return system_settings_form($form);
}
