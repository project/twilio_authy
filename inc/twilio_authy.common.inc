<?php
/**
 * Twilio authy common functions.
 */

/**
 * Twilio supported languages.
 */
function _twilio_authy_get_locale() {
  return array(
    'af' => 'Afrikaans',
    'ca' => 'Arabic',
    'ca' => 'Catalan',
    'zh' => 'Chinese',
    'zh-CN' => 'Chinese (Mandarin)',
    'zh-HK' => 'Chinese (Cantonese)',
    'hr' => 'Croatian',
    'cs' => 'Czech',
    'da' => 'Danish',
    'nl' => 'Dutch',
    'en' => 'English',
    'fi' => 'Finnish',
    'fr' => 'French',
    'de' => 'German',
    'el' => 'Greek',
    'he' => 'Hebrew',
    'hi' => 'Hindi',
    'hu' => 'Hungarian',
    'id' => 'Indonesian',
    'it' => 'Italian',
    'ja' => 'Japanese',
    'ko' => 'Korean',
    'ms' => 'Malay',
    'nb' => 'Norwegian',
    'pl' => 'Polish',
    'pt-BR' => 'Portuguese - Brazil',
    'pt' => 'Portuguese',
    'ro' => 'Romanian',
    'ru' => 'Russian',
    'es' => 'Spanish',
    'sv' => 'Swedish',
    'tl' => 'Tagalog',
    'th' => 'Thai',
    'tr' => 'Turkish',
    'vi' => 'Vietnamese',
  );
}
