<?php
/**
 * @file
 * Twilio authy debug functions.
 */

/**
 * Twilio authy debug form.
 */
function twilio_authy_debug_form($form, &$form_state) {
  module_load_include('inc', 'twilio_authy', 'inc/twilio_authy.common');
  $form['#tree'] = TRUE;
  // Debug create authy user section.
  $form['user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Create authy user'),
    '#weight' => 1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['user']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Authy user mail'),
  );
  $form['user']['cellphone'] = array(
    '#type' => 'textfield',
    '#title' => t('Authy user cellphone'),
  );
  $form['user']['country_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Authy user country code'),
    '#description' => t('Numeric calling country code of the country Eg: 1 for the US. 91 for India. 54 for Mexico.'),
  );
  $form['user']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
    '#validate' => array('twilio_authy_debug_form_user_creation_validate'),
    '#submit' => array('twilio_authy_debug_form_user_creation_submit'),
  );
  // Debug authy opt send section.
  $form['otp'] = array(
    '#type' => 'fieldset',
    '#title' => t('Send OTP to authy user'),
    '#weight' => 2,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  // TODO: dropdown?
  $form['otp']['locale'] = array(
    '#type' => 'select',
    '#title' => t('Locale'),
    '#options' => _twilio_authy_get_locale(),
    '#description' => t('The language of the message received by user. If no locale is given, Twilio will try to autodetect it based on the country code. English is used if no locale is autodetected. See !detail.', array(
      '!detail' => l(t('More details'), 'https://www.twilio.com/docs/authy/api/one-time-passwords#supported-languages', array(
        'attributes' => array(
          'target' => '_blank',
        ),
      )),
    )),
  );
  $form['otp']['authy_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Authy id'),
  );
  $form['otp']['force'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force send SMS'),
    '#description' => t('If the user has the Authy App, by default, the API will not send the 2FA code via SMS or voice. You can override this behavior and force sending an SMS or Voice call.'),
  );
  $form['otp']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
    '#validate' => array('twilio_authy_debug_form_otp_send_validate'),
    '#submit' => array('twilio_authy_debug_form_otp_send_submit'),
  );
  // Debug opt verify section.
  $form['verify'] = array(
    '#type' => 'fieldset',
    '#title' => t('Verify OTP'),
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['verify']['token'] = array(
    '#type' => 'textfield',
    '#title' => t('OTP token'),
  );
  $form['verify']['authy_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Authy id'),
  );
  $form['verify']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Verify'),
    '#validate' => array('twilio_authy_debug_form_otp_verify_validate'),
    '#submit' => array('twilio_authy_debug_form_otp_verify_submit'),
  );
  return $form;
}

/**
 * Twilio authy debug form user creation submit.
 */
function twilio_authy_debug_form_user_creation_validate($form, &$form_state) {
  if (empty($form_state['values']['user']['email'])) {
    form_set_error('user][email', t('@label is required.', array(
      '@label' => $form['user']['email']['#title'],
    )));
  }
  if (empty($form_state['values']['user']['cellphone'])) {
    form_set_error('user][cellphone', t('@label is required.', array(
      '@label' => $form['user']['cellphone']['#title'],
    )));
  }
  if (empty($form_state['values']['user']['country_code'])) {
    form_set_error('user][country_code', t('@label is required.', array(
      '@label' => $form['user']['country_code']['#title'],
    )));
  }
}

/**
 * Twilio authy debug form otp send validate.
 */
function twilio_authy_debug_form_otp_send_validate($form, &$form_state) {
  if (empty($form_state['values']['otp']['locale'])) {
    form_set_error('otp][locale', t('@label is required.', array(
      '@label' => $form['otp']['locale']['#title'],
    )));
  }
  if (empty($form_state['values']['otp']['authy_id'])) {
    form_set_error('otp][authy_id', t('@label is required.', array(
      '@label' => $form['otp']['authy_id']['#title'],
    )));
  }
}

/**
 * Twilio authy debug form otp verify validate.
 */
function twilio_authy_debug_form_otp_verify_validate($form, &$form_state) {
  if (empty($form_state['values']['verify']['token'])) {
    form_set_error('verify][token', t('@label is required.', array(
      '@label' => $form['verify']['token']['#title'],
    )));
  }
  if (empty($form_state['values']['verify']['authy_id'])) {
    form_set_error('verify][authy_id', t('@label is required.', array(
      '@label' => $form['verify']['authy_id']['#title'],
    )));
  }
}

/**
 * Twilio authy debug form user creation submit.
 */
function twilio_authy_debug_form_user_creation_submit($form, &$form_state) {
  $authy_api = new Authy\AuthyApi(variable_get('twilio_authy_x_authy_api_key'));
  $email = $form_state['values']['user']['email'];
  $cellphone = $form_state['values']['user']['cellphone'];
  $country_code = $form_state['values']['user']['country_code'];
  // Call twilio authy create user API.
  $authy_user = $authy_api->registerUser($email, $cellphone, $country_code);
  if ($authy_user->ok()) {
    drupal_set_message(t('User authy id: @id', array('@id' => $authy_user->id())));
  }
  else {
    foreach($authy_user->errors() as $field => $message) {
      drupal_set_message($field . ' : ' . $message, 'error');
    }
  }
}

/**
 * Twilio authy debug form otp send submit.
 */
function twilio_authy_debug_form_otp_send_submit($form, &$form_state) {
  $authy_api = new Authy\AuthyApi(variable_get('twilio_authy_x_authy_api_key'));
  $authy_id = $form_state['values']['otp']['authy_id'];
  $response = $authy_api->requestSms($authy_id, array(
    'force' => $form_state['values']['otp']['force'],
    'locale' => $form_state['values']['otp']['locale'],
  ));
  if ($response->ok()) {
    drupal_set_message($response->message());
  }
  else {
    foreach($response->errors() as $field => $message) {
      drupal_set_message($field . ' : ' . $message, 'error');
    }
  }
}

/**
 * Twilio authy debug form opt verify submit.
 */
function twilio_authy_debug_form_otp_verify_submit($form, &$form_state) {
  $token = $form_state['values']['verify']['token'];
  $authy_id = $form_state['values']['verify']['authy_id'];

  $authy_api = new Authy\AuthyApi(variable_get('twilio_authy_x_authy_api_key'));
  $response = $authy_api->verifyToken($authy_id, $token);
  if ($response->ok()) {
    drupal_set_message($response->message());
  }
  else {
    foreach($response->errors() as $field => $message) {
      drupal_set_message($field . ' : ' . $message, 'error');
    }
  }
}
